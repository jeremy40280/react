import React, { useState } from 'react';
import {Link} from "react-router-dom"
export default function LoginForm() {
    const ConnexionValidé = () => {
        alert("La connexion à bien été prise en compte")
    }
    return (
    <><div className="bg-white ms-3 me-3 d-flex flex-column align-items-center modal-header">

            <h1 className="text-center">FORMULAIRE DE CONNEXION</h1></div>
            <button className="btn btn-danger ms-3"> <Link to="/" style={{ textDecoration: 'none', color: 'white' }}> RETOUR AU MENU PRINCIPAL </Link> </button>

            <div>

                <form className="d-flex flex-column align-items-center bg-white ms-3 me-3 d-flex flex-column align-items-center modal-header" id="monFormulaire">
            <div>
                        <label htmlFor="email">EMAIL:   </label>
                        <input className='ms-2' type="email" id="email" name="email" placeholder="Votre email "></input>
            </div>
                    <br></br>
            <div>
                        <label htmlFor="password">MOT DE PASSE:</label>
                        <input className='ms-2' type="password" id="password" name="password" placeholder="Mot de Passe"></input>
            </div>
                    <button type="submit" className="mt-3" onClick={ConnexionValidé()}>SE CONNECTER</button>
                </form>
            </div>
    </>     
        
    );
}


