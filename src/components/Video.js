import React from 'react'
import ReactPlayer from 'react-player'
//import './Video.css'

type Props = {
    externalSource: string;
}

function Video(props: Props) {
    const {externalSource} = props;
    return(
        <div className="player-wrapper bg-secondary">
            <ReactPlayer 
            url={externalSource}
            controls
            playing 
            muted 
            width="20%" 
            height="20%" 
            className="player" 
           />
        </div>
    );
}
export default Video