import React from 'react'
import {Link} from "react-router-dom"
import {VIDEO_CATEGORIE} from "../utils/categorieEnum";

type Props = {
    user: boolean;
    onCategoryChange: any;
}

export default function Navbar(props: Props) {
    const {user, onCategoryChange} = props;
    const handleCategoryChange = (event) => {
        onCategoryChange(event.target.value);
    }
    return (
    <><nav className="navbar navbar-light bg-light px-4">
      <Link to="/" className="navbar-brand">
      VOICI NOTRE PROJET DE DIFFUSION YFLIX                       
      </Link>

        {user ? <button className="btn btn-danger ms-2"> <Link to="/login" style={{ textDecoration: 'none', color: 'white'  }} > SE DECONNECTER </Link> </button>
        : <div>
                <button className="btn btn-secondary"><Link to="/register" style={{ textDecoration: 'none', color: 'black' }} > S'INSCRIRE </Link> </button>
                <button className="btn btn-secondary ms-2"> <Link to="/login" style={{ textDecoration: 'none', color: 'black'  }} > SE CONNECTER </Link>  </button>
            </div>}
    </nav>
        <nav className="navbar navbar-light bg-secondary">
          <link to="/FILMS" className="navbar-brand"></link>
        <div className="navbar navbar-light ms-2">
          <button className="btn btn-danger ms-3"> CATEGORIES <br></br> <div class="dropdown">
              <select onChange={handleCategoryChange}>
                  <option value="" disabled selected>CHOIX DE CATEGORIES</option>
                  <option value={VIDEO_CATEGORIE.romantique}>{VIDEO_CATEGORIE.romantique}</option>
                  <option value={VIDEO_CATEGORIE.fantastique}>{VIDEO_CATEGORIE.fantastique}</option>
                  <option value={VIDEO_CATEGORIE.historique}>{VIDEO_CATEGORIE.historique}</option>
              </select>
    </div></button>
        </div>
        
        </nav>
        </>
    );
}
