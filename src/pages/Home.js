import React, {useContext, useState} from 'react'
import Video from "../components/Video";
import Navbar from "../components/Navbar";
import {video} from "../utils/videoUrl";

export default function Home() {
  const user = false;
  const [category, setCategory] = useState('');
  const handleCategoryChange = (newCategory) => {
      console.log(newCategory)
      setCategory(newCategory);
  }

  return (
    <>
        <Navbar  user={user} onCategoryChange={handleCategoryChange}/>
        <div className="container p-15 bg-danger">
      <br></br>
      <h1 className="display-15 text-light text-center">Hello, voici notre première page de connexion</h1>
      <p className="display-16 text-light text-center ">Le projet YFLIX a pour but de regrouper des vidéos et des rendus des différents projets étudiants des YDAYS</p>

    </div>
    <div>
    <span className="container p-2 menu navigation-menu bg-light">Menu</span>
    </div>
        {video.filter((vid) => vid.categorie === category).map((vid) => (
            <Video externalSource={vid.url} />
        ))}
    </>
  );
} 