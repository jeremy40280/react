import {VIDEO_CATEGORIE} from "./categorieEnum";

type Video = {
    url: string;
    categorie: VIDEO_CATEGORIE;
}

export const video: Video[] = [
    {url: "https://www.youtube.com/watch?v=Rx02_nUBte8", categorie: VIDEO_CATEGORIE.romantique},
    {url: "https://www.youtube.com/watch?v=Rx02_nUBte8", categorie: VIDEO_CATEGORIE.romantique},
    {url: "https://www.youtube.com/watch?v=Rx02_nUBte8", categorie: VIDEO_CATEGORIE.romantique},
    {url: "https://www.youtube.com/watch?v=Rx02_nUBte8", categorie: VIDEO_CATEGORIE.romantique},
    {url: "https://www.youtube.com/watch?v=Qq7Q6CLCfvs", categorie: VIDEO_CATEGORIE.historique},
    {url: "https://www.youtube.com/watch?v=Dw3BZ6O_8LY", categorie: VIDEO_CATEGORIE.fantastique}
]