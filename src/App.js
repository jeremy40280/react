
import {Routes, Route, HashRouter} from "react-router-dom"
import Home from "./pages/Home"
import Navbar from "./components/Navbar"
import Video from './components/Video'
import { useState } from "react"
import LoginPage from "./pages/Login";
import RegisterPage from "./pages/Register";
//import Slide from './components/Slide'
//import { hasAuthenticated } from "./services/AuthApi"
//import Auth from "./contexts/Auth"
//import AuthenticatedRoute from "./components/AuthenticatedRoute"



function App() {
    return (
    <div className="container-fluid">
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
        </Routes>
    </div>
  );
}

export default App;
